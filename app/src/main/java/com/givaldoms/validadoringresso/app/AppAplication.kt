package com.givaldoms.validadoringresso.app

import android.app.Application
import com.givaldoms.validadoringresso.app.di.appModule
import com.givaldoms.validadoringresso.data.repository.di.repositoryModule

import com.google.firebase.FirebaseApp
import org.koin.android.ext.android.startKoin

class AppAplication : Application() {

    override fun onCreate() {

        startKoin(this, listOf(appModule, repositoryModule))
        FirebaseApp.initializeApp(this)

        super.onCreate()
    }
}
