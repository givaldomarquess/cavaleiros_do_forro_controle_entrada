package com.givaldoms.validadoringresso.app.di

import com.givaldoms.validadoringresso.app.main.MainViewModel
import com.givaldoms.validadoringresso.data.interactor.Interactor
import com.givaldoms.validadoringresso.data.interactor.InteractorImpl
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {

    single { InteractorImpl(get(), get()) as Interactor }

    viewModel { MainViewModel(get()) }

}