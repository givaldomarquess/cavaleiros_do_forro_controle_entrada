package com.givaldoms.validadoringresso.app.main

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import com.givaldoms.validadoringresso.R
import com.givaldoms.validadoringresso.app.model.User
import com.givaldoms.validadoringresso.barcodescanning.BarcodeDectedListener
import com.givaldoms.validadoringresso.barcodescanning.BarcodeScanningProcessor
import com.givaldoms.validadoringresso.common.CameraSource
import com.givaldoms.validadoringresso.data.repository.remote.model.UserResult
import com.givaldoms.validadoringresso.databinding.ActivityMainBinding
import com.givaldoms.validadoringresso.utils.extenstions.putText
import com.google.firebase.ml.common.FirebaseMLException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main_user_info.*
import org.jetbrains.anko.apply
import org.jetbrains.anko.defaultSharedPreferences
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException

class MainActivity : AppCompatActivity(), BarcodeDectedListener<String> {

    private val mViewModel: MainViewModel by viewModel()

    private lateinit var mBinding: ActivityMainBinding

    private var cameraSource: CameraSource? = null
    private lateinit var mBottomSheetBehavior: BottomSheetBehavior<View>

    private val requiredPermissions: Array<String?>
        get() {
            return try {
                val info = this.packageManager
                        .getPackageInfo(this.packageName, PackageManager.GET_PERMISSIONS)
                val ps = info.requestedPermissions
                if (ps != null && ps.isNotEmpty()) ps else arrayOfNulls(0)
            } catch (e: Exception) {
                arrayOfNulls(0)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mBinding.vm = mViewModel

        setSupportActionBar(mainToolbar)

        mViewModel.getUser()?.observe(this, Observer<User> { user ->
            if (user == null || !user.hasAccess) {
                imageView.setImageDrawable(getDrawable(R.drawable.ic_failure_red_24dp))
            } else {
               imageView.setImageDrawable(getDrawable(R.drawable.ic_success_green_24dp))
            }

        })

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet as View).apply {
            state = BottomSheetBehavior.STATE_COLLAPSED
            isHideable = false
            isFitToContents = true
            peekHeight = 700
            setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(p0: View, p1: Float) {}

                override fun onStateChanged(p0: View, p1: Int) {
                    if (p1 != BottomSheetBehavior.STATE_COLLAPSED) {
                        firePreview.stop()
                    } else {
                        startCameraSource()
                    }
                }

            })
        }



        if (allPermissionsGranted()) {
            createCameraSource()
        } else {
            getRuntimePermissions()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setMessage("URL do servidor")

        val text = defaultSharedPreferences.getString(getString(R.string.saved_base_url), "")
        val input = EditText(this)
        input.putText(text?: "")
        input.hint = "192.160.0.1"
        val lp = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT)
        input.layoutParams = lp
        alertDialog.setView(input)
                .setPositiveButton("Salvar") { _, _ ->
                    defaultSharedPreferences.apply {
                        putString(getString(R.string.saved_base_url), input.text.toString().trim())
                    }
                }.setNegativeButton("Cancelar") { _, _ ->

                }.show()

        return true
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        firePreview.stop()
    }

    public override fun onDestroy() {
        super.onDestroy()
        cameraSource?.release()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (allPermissionsGranted()) {
            createCameraSource()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun getRuntimePermissions() {
        val allNeededPermissions = ArrayList<String>()
        for (permission in requiredPermissions) {
            permission?.let {
                if (!isPermissionGranted(this, it)) {
                    allNeededPermissions.add(it)
                }
            }
        }

        if (!allNeededPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(
                    this, allNeededPermissions.toTypedArray(), PERMISSION_REQUESTS)
        }
    }

    private fun allPermissionsGranted(): Boolean {
        for (permission in requiredPermissions) {
            permission?.let {
                if (!isPermissionGranted(this, it)) return false
            }
        }
        return true
    }

    private fun createCameraSource() {
        if (cameraSource == null) {
            cameraSource = CameraSource(this, fireFaceOverlay)
        }
        try {
            cameraSource?.setMachineLearningFrameProcessor(BarcodeScanningProcessor(this))
        } catch (e: FirebaseMLException) {
            Log.e(TAG, "can not create camera source")
        }

        startCameraSource()
    }

    override fun onDetect(content: String) {
        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        mViewModel.onNewCode(content)
    }

    private fun startCameraSource() {
        cameraSource.let {
            try {
                if (firePreview == null) {
                    Log.d(TAG, "resume: Preview is null")
                }
                if (fireFaceOverlay == null) {
                    Log.d(TAG, "resume: graphOverlay is null")
                }
                firePreview.start(it, fireFaceOverlay)
            } catch (e: IOException) {
                Log.e(TAG, "Unable to start camera source.", e)
                it?.release()
                cameraSource = null
            }

        }
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val PERMISSION_REQUESTS = 1

        private fun isPermissionGranted(context: Context, permission: String): Boolean {
            if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission granted: $permission")
                return true
            }
            Log.i(TAG, "Permission NOT granted: $permission")
            return false
        }
    }
}
