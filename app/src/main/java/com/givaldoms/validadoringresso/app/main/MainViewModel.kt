package com.givaldoms.validadoringresso.app.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import android.util.Log
import com.givaldoms.validadoringresso.app.model.User
import com.givaldoms.validadoringresso.data.interactor.Interactor
import com.givaldoms.validadoringresso.utils.extenstions.UI
import com.givaldoms.validadoringresso.utils.extenstions.utcToDataHora
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

class MainViewModel(private val interactor: Interactor) : ViewModel() {

    val name = ObservableField<String>()
    val rg = ObservableField<String>()
    val message = ObservableField<String>().also { it.set("Aguardando leitura do código") }
    val time = ObservableField<String>()
    val isLoading = ObservableField<Boolean>()
    var mJob: Job? = null
    var user: MutableLiveData<User>? = null

    fun getUser(): LiveData<User>? {
        if (user == null) {
            user = MutableLiveData()
            getUser()
        }
        return user
    }

    fun onNewCode(code: String) {
//        Log.d("MainViewModel", code)
        if (mJob?.isActive == true) return

        isLoading.set(true)

        @Suppress("EXPERIMENTAL_FEATURE_WARNING")
        mJob = launch {
            delay(2000)
            val result = interactor.validateCode(code)

            launch(UI) {
                time.set("")
                rg.set("")
                name.set("")

                result.success?.let {
                    name.set("Nome: ${it.nome}")
                    time.set("Entrada: ${it.horaEntrada?.utcToDataHora()}")
                    rg.set("RG: ${it.rg}")
                }

                user?.postValue(result.success)

                message.set(
                        when {
                            result.failure != null -> result.failure.message ?: "Erro desconhecido"
                            result.success != null -> "Entrada permitida"
                            else -> "Erro desconhecido"
                        }
                )
                isLoading.set(false)
            }
        }
    }

}
