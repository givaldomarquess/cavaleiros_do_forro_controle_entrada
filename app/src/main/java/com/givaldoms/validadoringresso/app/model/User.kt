package com.givaldoms.validadoringresso.app.model

data class User(
        val ui :String,
        val rg: String,
        val nome: String,
        val horaEntrada: String?,
        var hasAccess: Boolean = false)
