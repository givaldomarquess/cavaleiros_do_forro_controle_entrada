package com.givaldoms.validadoringresso.barcodescanning

interface BarcodeDectedListener<in C> {

    fun onDetect(content: C)

}