package com.givaldoms.validadoringresso.base

interface AdapterItemClickListener<in T> {

    fun onItemClick(item: T) = Unit

    fun onItemLongClick(item: T)= true

}
