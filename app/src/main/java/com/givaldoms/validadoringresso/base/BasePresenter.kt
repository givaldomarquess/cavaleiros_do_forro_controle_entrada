package com.givaldoms.validadoringresso.base

/**
 * Created by givaldoms on 28/05/2018.
 */
interface BasePresenter<V> {

    val view: V

    fun stop()

}

interface HandlerPresenter {
    fun handlerException(ex: Exception)
}
