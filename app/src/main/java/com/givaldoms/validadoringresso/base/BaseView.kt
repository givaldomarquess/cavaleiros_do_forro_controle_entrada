package com.givaldoms.validadoringresso.base

/**
 * Created by givaldoms on 28/05/2018.
 */
interface BaseView <out T: BasePresenter<*>>: MessageView {

    val presenter: T
}