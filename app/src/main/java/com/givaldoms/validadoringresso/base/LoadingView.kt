package com.givaldoms.validadoringresso.base

/**
 * Created by givaldoms on 28/05/2018.
 */
interface LoadingView {

    fun showProgress()

    fun hideProgress()
}