package com.givaldoms.validadoringresso.base

import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast
import com.givaldoms.validadoringresso.R

/**
 * Created by givaldoms on 28/05/2018.
 */
interface MessageView {

    val mContext: Context

    fun showMessage(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessage(@StringRes message: Int) {
        Toast.makeText(mContext, mContext.getString(message), Toast.LENGTH_SHORT).show()
    }

}