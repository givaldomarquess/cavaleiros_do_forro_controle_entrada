package com.givaldoms.validadoringresso.base

/**
 * Created by givaldoms on 27/06/2018.
 */
interface ReposListener<S, F> {

    fun onSuccess(s: S)

    fun onFailure(f: F)

}