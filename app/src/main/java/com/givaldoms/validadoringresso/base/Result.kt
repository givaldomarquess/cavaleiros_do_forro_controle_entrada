package com.givaldoms.validadoringresso.base

import kotlinx.coroutines.experimental.Deferred

suspend fun <T> Deferred<T>.awaitResult(): Result<T> = try {
    Result(await(), null)
} catch (e: Exception) {
    Result(null, e)
}

data class Result<out T>(
        val success: T?,
        val failure: Exception?)
