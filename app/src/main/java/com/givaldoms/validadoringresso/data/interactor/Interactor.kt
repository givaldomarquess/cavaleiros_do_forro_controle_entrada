package com.givaldoms.validadoringresso.data.interactor

import android.content.SharedPreferences
import com.givaldoms.validadoringresso.app.model.User
import com.givaldoms.validadoringresso.base.Result
import com.givaldoms.validadoringresso.data.repository.remote.RemoteDataSource
import com.givaldoms.validadoringresso.data.repository.remote.model.UserResult

interface Interactor {

    suspend fun validateCode(code: String): Result<User>

}

class InteractorImpl(private val remote: RemoteDataSource,
                     private val preferences: SharedPreferences): Interactor {

    override suspend fun validateCode(code: String): Result<User> {
        return try {
            val url = preferences.getString("SAVED_BASE_URL", null)
                    ?: return Result(null, java.lang.Exception("Url não informada"))
            remote.pathUser("$url/$code")
        } catch (e: Exception) {
            Result(null, e)
        }
    }
}