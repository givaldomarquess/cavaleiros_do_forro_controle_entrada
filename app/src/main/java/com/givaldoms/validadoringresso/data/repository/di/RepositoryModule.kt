package com.givaldoms.validadoringresso.data.repository.di

import com.givaldoms.validadoringresso.data.repository.remote.*
import org.jetbrains.anko.defaultSharedPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.applicationContext

val repositoryModule = applicationContext {

    bean { createOkHttpClient() }
    bean { createWebService<WebService>(get(), DatasourceProperties.SERVER_URL) }
    bean { RemoteDataSourceImpl(get()) as RemoteDataSource }

    bean { androidContext().defaultSharedPreferences }

}
