package com.givaldoms.validadoringresso.data.repository.remote

import com.givaldoms.validadoringresso.app.model.User
import com.givaldoms.validadoringresso.base.Result
import com.givaldoms.validadoringresso.base.awaitResult
import com.givaldoms.validadoringresso.data.repository.remote.model.UserResult
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.Exception
import org.json.JSONObject

interface RemoteDataSource {

    suspend fun pathUser(url: String): Result<User>

}

class RemoteDataSourceImpl(private val webService: WebService)
    : RemoteDataSource {

    override suspend fun pathUser(url: String): Result<User> {
        val uResult = webService.patchUser(url).awaitResult()

        val resultSuccess = uResult.success

        return if (resultSuccess != null) {
            when(resultSuccess.code()) {
                200 -> {
                    Result(resultSuccess.body()?.parseUser(true), null)
                }

                403 -> {
                    val jObjError = JSONObject(resultSuccess.errorBody()?.string())
                    val user: UserResult? = Gson().fromJson(jObjError.toString(), object : TypeToken<UserResult>(){}.type)
                    Result(user?.parseUser(false), Exception("Usuário já entrou"))
                }

                404, 500 -> Result(null, Exception("Usuário não encontrado"))
                else -> Result(null, Exception("Erro desconhecido"))
            }

        } else {
            Result(null, uResult.failure)
        }
    }
}