package com.givaldoms.validadoringresso.data.repository.remote

import com.givaldoms.validadoringresso.data.repository.remote.model.UserResult
import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import retrofit2.http.PATCH
import retrofit2.http.Url

interface WebService {

    @PATCH
    fun patchUser(@Url url: String): Deferred<Response<UserResult>>

}