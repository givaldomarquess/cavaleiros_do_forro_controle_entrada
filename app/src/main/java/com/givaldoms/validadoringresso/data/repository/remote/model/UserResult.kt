package com.givaldoms.validadoringresso.data.repository.remote.model

import com.givaldoms.validadoringresso.app.model.User


class UserResult(
        val ui :String?,
        val nome: String?,
        val rg: String?,
        val orgEmissor: String?,
        val whatsapp: String?,
        val instagran: String?,
        val sexo: String?,
        val dataNascimento: String?,
        val horaEntrada: String?,
        val cidade: String?,
        val bairro: String?,
        val indicacao: String?,
        val pushId: String?,
        val sorteado: Boolean?,
        val createdAt: String?,
        val updatedAt: String?) {

    fun parseUser(access: Boolean) = User(
            ui ?: "", rg ?: "", nome ?: "", horaEntrada, access
    )

}