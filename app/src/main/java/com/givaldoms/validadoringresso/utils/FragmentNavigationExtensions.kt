package com.givaldoms.validadoringresso.utils

import android.content.Context
import android.support.annotation.IdRes
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.View

fun AppCompatActivity.replaceFragment(@IdRes container: Int, fragment: () -> Fragment) {
    supportFragmentManager
            .beginTransaction()
            .replace(container, fragment())
            .commit()
}

fun AppCompatActivity.addFragmentStack(@IdRes container: Int, tag: String, fragment: () -> Fragment) {
    val f = supportFragmentManager.findFragmentByTag(tag)
    supportFragmentManager
            .beginTransaction()
            .add(container, f?: fragment())
            .addToBackStack(null)
            .commit()
}

fun<T: View> NavigationView.find(@IdRes id: Int, index: Int = 0) = getHeaderView(index).findViewById<T>(id)

fun DrawerLayout.close() {
    this.closeDrawer(GravityCompat.START)
}

fun Context.setTitle(title: String) {
    val act = this as? AppCompatActivity ?: return
    val actionBar = act.supportActionBar ?: return
    actionBar.title = title
}

fun Fragment.setTitle(title: String) {
    this.context?.setTitle(title)
}