package com.givaldoms.validadoringresso.utils.extenstions

import android.graphics.Bitmap
import android.graphics.Canvas


fun mergeBitmap(fr: Bitmap, sc: Bitmap): Bitmap {

    val comboBitmap: Bitmap

    val width: Int = fr.width + sc.width
    val height: Int = fr.height

    comboBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

    val comboImage = Canvas(comboBitmap)

    comboImage.drawBitmap(fr, 0f, 0f, null)
    comboImage.drawBitmap(sc, fr.width.toFloat(), 0f, null)
    return comboBitmap

}
