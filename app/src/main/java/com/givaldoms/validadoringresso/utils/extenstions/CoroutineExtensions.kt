package com.givaldoms.validadoringresso.utils.extenstions

import kotlinx.coroutines.experimental.CoroutineExceptionHandler
import timber.log.Timber


val UI = kotlinx.coroutines.experimental.android.UI + CoroutineExceptionHandler { _, e ->
    Timber.d(e)
    throw e
}