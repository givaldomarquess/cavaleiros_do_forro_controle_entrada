package com.givaldoms.validadoringresso.utils.extenstions

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner

fun View.setVisible(v: Boolean) = when (v) {
    true -> this.visibility = View.VISIBLE
    else -> this.visibility = View.GONE
}

fun Spinner.fill(list: List<String>) {
    val ctx = this.context
    val adapter = ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, list)
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

    this.adapter = adapter

}

fun Spinner.onItemSelected(f: (Any) -> Unit) {

    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {}

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            p0?.getItemAtPosition(p2)?.let {
                f(it)
            }
        }
    }

}
