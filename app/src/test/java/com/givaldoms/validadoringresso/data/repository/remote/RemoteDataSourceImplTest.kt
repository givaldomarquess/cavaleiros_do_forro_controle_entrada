package com.givaldoms.validadoringresso.data.repository.remote

import kotlinx.coroutines.experimental.launch
import org.junit.Test

import org.junit.Assert.*

class RemoteDataSourceImplTest {

    private val webService = createWebService<WebService>(
            createOkHttpClient(),
            DatasourceProperties.SERVER_URL)

    private val dataSource = RemoteDataSourceImpl(webService)

    @Test
    fun getUser() {
        launch {
            val result = dataSource.getUser("12341651355")
            println(result.success?.toString())
            assert(result.failure != null)
        }
    }

    @Test
    fun pathUser() {
    }
}